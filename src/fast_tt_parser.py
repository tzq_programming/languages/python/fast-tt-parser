import requests
from bs4 import BeautifulSoup
from urllib.request import urlopen, Request
from urllib.parse import urlencode
from slugify import slugify

URL = 'http://raspisanie.bf.pstu.ru/'
URL_POST = "http://raspisanie.bf.pstu.ru/RepRasp.asp?" \
           "action=%u0421%u0444%u043E%u0440%u043C%u0438%u0440%u043E%u0432%u0430%u0442%u044C"
URL_ENCODING = 'cp1251'
DATE_FROM_NAME = 'ctl_dateFrom'
DATE_TO_NAME = 'ctl_dateTo'
ROW_CLASS = 'reportitem'
DATE_TO_NAME_VALUE = '01.07.2999'
SELECT_NAME = 'Name1'
CTL_N0X1 = 'ctl_n0x1'
CTL_N0X2 = 'ctl_n0x2'
CTL_N0X_VALUE = "{'U','0','0','0','0','0',''}"


class FastTTParser:
    """ Класс для парсинга данных с http://raspisanie.bf.pstu.ru/
    :param ctl_dateFrom - начало периода
    :param ctl_dateTo - конец периода
    :param options - список преподавателей
    """

    def __init__(self):
        self.ctl_dateFrom = ''
        self.ctl_dateTo = ''
        self.options = {}

        self.__get_main_data()

    def __get_main_data(self):
        """
        Метод, в котором парсятся основные данные - период и список преподователей
        """
        # получение данных
        try:
            req = requests.get(URL)
            req.encoding = URL_ENCODING
        except Exception as err:
            raise Exception('Отсутствует подключение')

        # парсинг значений
        soup = BeautifulSoup(req.text, 'lxml')
        self.ctl_dateFrom = soup.find('input', attrs={'name': DATE_FROM_NAME})['value']
        self.ctl_dateTo = soup.find('input', attrs={'name': DATE_TO_NAME})['value']
        options = list(map(lambda x: x.text, soup.find('select', attrs={'id': SELECT_NAME}).find_all('option')))

        # вызов ошибки, если нет данных
        if not (self.ctl_dateFrom and self.ctl_dateTo and options):
            raise Exception('Данные не обновлены')

        # создание ключа
        options_zip = list(map(lambda x: slugify(x, separator='-'), options))

        # подготовка данных
        for i in range(len(options)):
            self.options[options_zip[i]] = {
                'name': options[i],
                'data': []
            }

    def logger(func):
        def wrapper(*args, **kwargs):
            print(f'Parsing {args[0].options[args[1]]["name"]}')
            func(*args, **kwargs)

        return wrapper

    @logger
    def __get_timetable(self, key: str):
        """Получение расписания по key
        :param key: преподаватель/группа
        """
        # данные для запроса
        data = {
            DATE_FROM_NAME: self.ctl_dateFrom,
            DATE_TO_NAME: DATE_TO_NAME_VALUE,
            SELECT_NAME: self.options[key]['name'],
            CTL_N0X1: CTL_N0X_VALUE,
            CTL_N0X2: CTL_N0X_VALUE
        }

        # получение данных
        req = Request(URL_POST)
        req.data = urlencode(data, encoding=URL_ENCODING).encode(URL_ENCODING)
        req = urlopen(req)
        req = req.read().decode(URL_ENCODING)

        # парсинг + запись в options
        soup = BeautifulSoup(req, 'lxml')
        rows = soup.find_all('tr', attrs={'class': ROW_CLASS})
        for row in rows:
            _row = row.find_all('td')
            _row = list(map(lambda td: td.text.strip(), _row))
            self.options[key]['data'].append(_row)

    def get_all_timetable(self, option=None, date_start=None):
        """Метод для получения всего расписания
        :param date_start:
        :param option: получение расписания конкретного препода/группы
        """
        if date_start:
            self.ctl_dateFrom = date_start

        if not option:
            for key in self.options:
                self.__get_timetable(key)

            self.options['last_update'] = self.ctl_dateTo
        else:
            self.__get_timetable(option)
