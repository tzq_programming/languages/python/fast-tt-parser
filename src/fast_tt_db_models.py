from peewee import *
from .secret import db_connection as dbc

db = MySQLDatabase(
    dbc.db_name,
    user=dbc.db_user,
    password=dbc.db_password,
    host=dbc.db_host,
    port=dbc.db_port
)


class FBaseModel(Model):
    class Meta:
        database = db


class Tazaq_Fasttt_Options(FBaseModel):
    id = IntegerField(primary_key=True).auto_increment
    name = CharField(100)
    slug = CharField(100, unique=True)
    is_group = BooleanField(default=False)
    is_visible = BooleanField(default=False)


class Tazaq_Fasttt_Updates(FBaseModel):
    id = IntegerField(primary_key=True).auto_increment
    d_from = CharField(10)
    d_to = CharField(10)


class Tazaq_Fasttt_Services(FBaseModel):
    id = IntegerField(primary_key=True).auto_increment
    name = CharField(20, unique=True)


class Tazaq_Fasttt_Notifications(FBaseModel):
    id = IntegerField(primary_key=True).auto_increment
    d_from = DateField()
    d_to = DateField()
    option_id = ForeignKeyField(Tazaq_Fasttt_Options)
    user_id = CharField(191)
    service_id = ForeignKeyField(Tazaq_Fasttt_Services)


class Tazaq_Fasttt_Weekdays(FBaseModel):
    id = IntegerField(primary_key=True).auto_increment
    name = CharField(20, unique=True)


class Tazaq_Fasttt_Times(FBaseModel):
    id = IntegerField(primary_key=True).auto_increment
    name = CharField(20, unique=True)


class Tazaq_Fasttt_Subjects(FBaseModel):
    id = IntegerField(primary_key=True).auto_increment
    name = CharField(20, unique=True)


class Tazaq_Fasttt_Types(FBaseModel):
    id = IntegerField(primary_key=True).auto_increment
    name = CharField(20, unique=True)


class Tazaq_Fasttt_Classrooms(FBaseModel):
    id = IntegerField(primary_key=True).auto_increment
    name = CharField(20, unique=True)


class Tazaq_Fasttt_Lessons(FBaseModel):
    id = IntegerField(primary_key=True).auto_increment
    l_date = DateField()
    weekday_id = ForeignKeyField(Tazaq_Fasttt_Weekdays)
    start_id = ForeignKeyField(Tazaq_Fasttt_Times)
    finish_id = ForeignKeyField(Tazaq_Fasttt_Times)
    l_num = IntegerField(10)
    group_id = ForeignKeyField(Tazaq_Fasttt_Options)
    name_id = ForeignKeyField(Tazaq_Fasttt_Subjects)
    type_id = ForeignKeyField(Tazaq_Fasttt_Types)
    classroom_id = ForeignKeyField(Tazaq_Fasttt_Classrooms)
    teacher_id = ForeignKeyField(Tazaq_Fasttt_Options)
    hash_row = CharField(32, unique=True)