from datetime import datetime

template = """<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">   
    {0}
</urlset>
"""

url_template = """
    <url>
        <loc>{0}</loc>
        <lastmod>{1}</lastmod>
        <changefreq>{2}</changefreq>
        <priority>{3}</priority>
    </url>
"""


class SitemapGenerator:

    def __init__(self):
        from .fast_tt_db_models import db, Tazaq_Fasttt_Options as FOptions, Tazaq_Fasttt_Classrooms as FClassroom

        self.options = FOptions.select(FOptions.slug, FOptions.is_group).dicts()
        self.classrooms = FClassroom.select(FClassroom.name).dicts()

    def generateAndSave(self):
        url = 'https://fast-tt.ru/'
        menu = ['', 'classrooms', 'groups', 'teachers', 'donations']
        last_update = datetime.today().strftime('%Y-%m-%d')

        all_urls = """"""

        for item in menu:
            all_urls += url_template.format(url + item, last_update, 'weekly', 1)

        for item in list(self.options):
            all_urls += url_template.format(
                url + ('groups/' if item['is_group'] else 'teachers/') + item['slug'],
                last_update,
                'yearly',
                0.5
            )
            all_urls += url_template.format(
                url + 'timetable/' + ('groups/' if item['is_group'] else 'teachers/') + item['slug'],
                last_update,
                'weekly',
                0.5
            )

        for item in list(self.classrooms):
            all_urls += url_template.format(
                url + 'classrooms/' + item['name'],
                last_update,
                'yearly',
                0.5
            )
            all_urls += url_template.format(
                url + 'timetable/' + 'classrooms/' + item['name'],
                last_update,
                'weekly',
                0.5
            )

        with open('.sitemap.xml', 'w') as f:
            f.write(template.format(all_urls))