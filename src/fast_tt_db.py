from .fast_tt_db_models import db, Tazaq_Fasttt_Updates as FUpdates, Tazaq_Fasttt_Options as FOptions, \
    Tazaq_Fasttt_Classrooms as FClassrooms, Tazaq_Fasttt_Times as FTimes, \
    Tazaq_Fasttt_Types as FTypes, Tazaq_Fasttt_Subjects as FSubjects, Tazaq_Fasttt_Lessons as FLessons, \
    Tazaq_Fasttt_Weekdays as FWeekdays
from hashlib import md5
from datetime import datetime, date
from slugify import slugify


class FastTTDBWriter:
    def __init__(self):
        db.connect()

        self.data = {}

    def load(self, parser_data):
        """Метод загрузки данных из парсера
        :param parser_data:
        """
        self.data = parser_data

    @staticmethod
    def check_update(date_from, date_to):
        """Метод проверяющий обновления
        :param date_from:
        :param date_to:
        :return:
        """
        try:
            data = FUpdates.select().order_by(FUpdates.id.desc()).get()
        except Exception as err:
            return True

        return not (date_from == data.d_from and date_to == data.d_to)

    def save_options(self):
        """Метод сохранения списка преподов/групп в БД
        """
        # "Обнуление"
        FOptions.update(is_visible=False)

        # добавление новых option базу, если совпадения, то только обновляем видимость
        data = self.data.items()
        new_options = [
            {
                FOptions.name: val['name'],
                FOptions.slug: slugify(key, separator='-'),
                FOptions.is_group: len(val['name']) <= 15,
                FOptions.is_visible: True,
             }
            for key, val in data if key != 'last_update'
        ]
        FOptions.insert_many(new_options).on_conflict(update={FOptions.is_visible: True}).execute()

    def save_lessons(self):
        """Метод сохранения занятий в БД
        """
        # {slug: id}
        options = FOptions.select(FOptions.slug, FOptions.id).tuples()
        options = {key: val for key, val in options}

        # формирование массива занятий
        to_db = []
        for key, val in self.data.items():
            if key == 'last_update':
                continue

            data = val['data']

            # во время цикла берутся или создаются новые поля в справочниках
            for lesson in data:
                lesson_str = "".join(list(map(str, lesson)))
                _date, _weekday = self.__get_date_and_weekday(lesson[0])

                row = {
                    'l_date': _date,
                    'weekday_id': self.__get_id(FWeekdays, _weekday),
                    'start_id': self.__get_id(FTimes, lesson[1].rjust(5, '0')),
                    'finish_id': self.__get_id(FTimes, lesson[2].rjust(5, '0')),
                    'l_num': lesson[3],
                    'group_id': options.get(slugify(lesson[4], separator='-')),
                    'name_id': self.__get_id(FSubjects, lesson[5]),
                    'type_id': self.__get_id(FTypes, lesson[6]),
                    'classroom_id': self.__get_id(FClassrooms, slugify(lesson[7], separator='-'), True),
                    'teacher_id': options.get(slugify(lesson[8], separator='-')),
                    'hash_row': md5(bytes(lesson_str, 'utf-8')).hexdigest(),
                }

                to_db.append(row)

        # запись занятий в базу
        FLessons.insert_many(to_db).on_conflict_replace().execute()

    @staticmethod
    def write_update(date_from, date_to):
        """Метод для записи обновлений в лог
        :param date_from:
        :param date_to:
        """
        FUpdates.create(d_from=date_from, d_to=date_to)

    @staticmethod
    def __get_id(model, name, is_slug=False):
        """Метод извлекающий id из модели, либо создаёт новую и извлекает
        :param model:
        :param name:
        :return: id
        """
        if is_slug:
            name = slugify(name)
        model, created = model.get_or_create(name=name)
        if model != 0:
            return model.id
        else:
            return created.id

    @staticmethod
    def __get_date_and_weekday(string) -> (datetime, str):
        """Метод преобразования 15.06.20 (Понедельник) в 2020-06-15 и (Понедельник)
        :param string: строка от парсера
        :return: 2020-06-15 и (Понедельник)
        """
        splited_date = string.split(' ')
        _date = splited_date[0]
        _date = _date[:6] + '20' + _date[6:]
        _date = datetime.strptime(_date, '%d.%m.%Y').date()
        weekday = splited_date[1]

        return _date, weekday

