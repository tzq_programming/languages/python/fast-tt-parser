import os.path as os_path
from os import remove
import json


def main():
    if db_writer.check_update(parser.ctl_dateFrom, parser.ctl_dateTo):
        if os_path.isfile('data.json'):
            data = json.loads(open('data.json').read())
        else:
            parser.get_all_timetable()
            data = parser.options
            with open('data.json', 'w') as f:
                json.dump(data, f)

        db_writer.load(data)
        db_writer.save_options()
        db_writer.save_lessons()
        db_writer.write_update(parser.ctl_dateFrom, parser.ctl_dateTo)

        remove('data.json')


if __name__ == "__main__":
    from src.fast_tt_parser import FastTTParser
    from src.fast_tt_db import FastTTDBWriter
    parser = FastTTParser()
    db_writer = FastTTDBWriter()

    main()
